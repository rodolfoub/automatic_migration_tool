//CSV file: http://papaparse.com/
let users_data = [];
//$(document).ready(function() {

  let myApp = angular.module('csvAutomation', ['toastr', 'ngStorage']);

  myApp.directive('customOnChange', function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        let onChangeFunc = scope.$eval(attrs.customOnChange);
        element.bind('change', function(event) {
          let files = event.target.files;
          onChangeFunc(files);
        });

        element.bind('click', function() {
          element.val('');
        });
      }
    };
  });

  myApp.controller('csvAutomationController', function($scope, $http, $sce, $q, $localStorage, $sessionStorage, toastr) {
    $scope.loader = false;
    $scope.zendesk = new Object();
    $scope.$storage = $localStorage;

    $scope.zendesk.APIkey = $scope.$storage.APIkey;
    $scope.zendesk.uri = $scope.$storage.uri;
    $scope.zendesk.user = $scope.$storage.user;
    $scope.userFieldsArray = [];
    $scope.customFieldsArray = [];
    $scope.orgFieldsArray = [];
    $scope.payload = [];
    $scope.blockLines = [];
    $scope.update_process = true;
    $scope.delay = 0;
    $scope.running = false;

    $("#zendesk-settings-modal").modal();

    $scope.zendeskSettings = function() {
      // console.log($scope.zendesk);
      $("#zendesk-settings-modal").modal('hide');
      $scope.loader = true;
      $http.post('/auth', $scope.zendesk).then(function(response) {
        $scope.loader = false;
        if (!response.data.user || !response.data.user.id)
          toastr.error("Zendesk Authentication Error");
        else {
          $scope.$storage.APIkey = $scope.zendesk.APIkey;
          $scope.$storage.uri = $scope.zendesk.uri;
          $scope.$storage.user = $scope.zendesk.user;
          toastr.success("Authenticated");
          switch ($scope.dataToBeUpdated) {
            case "createAndUpdateUsers":
            $scope.usersMethod = "updateOnly";
            $("#csv-users-settings-modal").modal();
            break;
            case "createTickets":
            $("#csv-tickets-settings-modal").modal();
            $scope.CSVColumns = {status: 'fixed:closed', tags: 'ticket_migrado'};
            break;
            case "createOrganizations":
            $("#csv-orgs-settings-modal").modal();
            break;
            default:
            break;
          }
        }
      }).catch(function(err) {
        console.error(err);
        toastr.error("Authentication error!");
      })
    }

    $scope.uploadFile = function(files) {
      let filename = files[0].name;
      alert('file was selected: ' + filename);
    };

    $scope.addUserField = function() {
      $scope.userFieldsArray.push({ id: "", value: "", dataType: "Text" });
    }

    $scope.addCustomField = function() {
      $scope.customFieldsArray.push({ id: "", value: "" });
    }

    $scope.addOrgField = function() {
      $scope.orgFieldsArray.push({ id: "", value: "" });
    }

    $scope.CSVSettings = function(files) {
      $scope.CSVFile = files[0];
      $scope.CSVInfo = $sce.trustAsHtml("<b>Reading CSV file. Please wait...</b>");
      $scope.$apply();
      Papa.parse($scope.CSVFile, {
        download: true,
        header: true,
        skipEmptyLines: true,
        complete: function(results) {
          console.log(results);
          $scope.CSVData = results.data;
          $scope.CSVHeaders = results.meta.fields;
          $scope.indexInit = 2;
          $scope.indexEnd = $scope.CSVData.length + 1;
          $scope.CSVInfo = $sce.trustAsHtml("<b>Finished reading CSV! " + $scope.CSVData.length + "</b> Lines | <b>" + $scope.CSVHeaders.length + "</b> Colums");
          $scope.$apply();
        }
      });
    }

    $scope.CSVstart = function() {
      console.dir($scope.CSVHeaders);
      console.dir($scope.CSVColumns);
      console.dir($scope.userFieldsArray);
      $scope.indexInit = $scope.indexInit <= 1 ? 2 :  $scope.indexInit;
      $scope.indexEnd = $scope.indexEnd > $scope.CSVData.length + 1 ? $scope.CSVData.length + 1 :  $scope.indexEnd;
      $scope.indexInit = Number($scope.indexInit);
      $scope.indexEnd = Number($scope.indexEnd);
      console.log('From %s to %s', $scope.indexInit, $scope.indexEnd);
      $("#csv-users-settings-modal").modal('hide');
      $("#csv-tickets-settings-modal").modal('hide');
      $("#csv-orgs-settings-modal").modal('hide');
      $scope.delay = Number($scope.delay);

      $scope.results = [];
      let promiseArray = [];
      let count = Math.ceil(($scope.indexEnd - $scope.indexInit) / 50000) || 1;
      displayExtraMessages("blue", "Starting process on file " + $scope.CSVFile.name);
      displayExtraMessages("deeppink", "Execution will be done in " + count + " parts");

      for (let i = 0; i < count; i++) {
        let ct = function() {
          return $q((resolve, reject) => {
            let index_start = $scope.indexInit - 2;
            let index_end = (index_start + 49999) >= $scope.indexEnd - 2 ? $scope.indexEnd - 2 : index_start + 49999;
            $scope.running = true;
            $scope.update_process = true;
            $scope.cb = function() {
              resolve(index_start + " > " + index_end);
            }
            console.log("Starting part #" + (Number(i) + 1));
            displayExtraMessages("", "Running part #" + (Number(i) + 1) + " | Rows " + (index_start + 2) + " to " + (index_end + 2));
            switch ($scope.dataToBeUpdated) {
              case "createAndUpdateUsers":
              createUsers($scope.CSVData, $scope.userFieldsArray, $scope.CSVColumns, index_start, index_end);
              break;
              case "createTickets":
              createTickets($scope.CSVData, $scope.customFieldsArray, $scope.CSVColumns, index_start, index_end);
              break;
              case "createOrganizations":
              createOrgs($scope.CSVData, $scope.orgFieldsArray, $scope.CSVColumns, index_start, index_end)
              break;
              default:
              break;
            }
            $scope.indexInit = index_end + 3;
          });
        }
        promiseArray.push(ct);
      }

      mapSeries(promiseArray).then(function() {
        displayExtraMessages("green", "Finished processing all selected rows");
        console.log($scope.results);
      });
    }

    $scope.toggleUpdate = function() {
      $scope.update_process = !$scope.update_process;
      if ($scope.update_process) {
        switch ($scope.dataToBeUpdated) {
          case "createAndUpdateUsers":
          sendUsersRequest()
          break;
          case "createTickets":
          sendTicketsRequest();
          break;
          case "createOrganizations":
          sendOrgsRequest();
          break;
          default:
          break;
        }
      }
    }

    $scope.print = function() {
      window.print();
    }

    function mapSeries(arr) {
      return arr.reduce(function(promise, item) {
        return promise
        .then(function(result) {
          return item().then(function(result) {
            $scope.results.push(result);
          });
        })
        .catch(console.error);
      }, Promise.resolve());
    }

    function createOrgs(csv, org_fields, columns, index_start, index_end) {
      $scope.payload.length = 0;
      $scope.blockLines.length = 0;
      let data, block = { "organizations": [] };
      $scope.blockLines = [{from:index_start + 2}];
      for (let i in csv) {
        if (Object.keys(csv[i]).length != $scope.CSVHeaders.length) continue;
        if ((i >= (index_start)) && (i <= (index_end))) {
          block.organizations.push(getOrgBody(csv[i], columns, org_fields));
        }
        if (block.organizations.length == 100) {
          $scope.blockLines[$scope.blockLines.length-1].to = Number(i)+2;
          $scope.blockLines.push({from:Number(i)+3})
          $scope.payload.push({ "organizations": block.organizations });
          block = { "organizations": [] }
        }
      }
      if (block.organizations.length) {
        $scope.blockLines[$scope.blockLines.length-1].to = index_end+2;
        $scope.payload.push({ "organizations": block.organizations });
      }
      if (!$scope.blockLines[$scope.blockLines.length-1].to)
        $scope.blockLines.pop();
      console.dir($scope.payload);
      console.dir($scope.blockLines);
      $scope.processIndex = 0;
      sendOrgsRequest();
    }

    function createTickets(csv, custom_fields, columns, index_start, index_end) {
      $scope.payload.length = 0;
      $scope.blockLines.length = 0;
      let data, block = { "tickets": [] };
      $scope.blockLines = [{from:index_start + 2}];
      for (let i in csv) {
        if (Object.keys(csv[i]).length != $scope.CSVHeaders.length) continue;
        if ((i >= (index_start)) && (i <= (index_end))) {
          block.tickets.push(getTicketBody(csv[i], columns, custom_fields));
        }
        if (block.tickets.length == 100) {
          $scope.blockLines[$scope.blockLines.length-1].to = Number(i)+2;
          $scope.blockLines.push({from:Number(i)+3})
          $scope.payload.push({ "tickets": block.tickets });
          block = { "tickets": [] }
        }
      }
      if (block.tickets.length) {
        $scope.blockLines[$scope.blockLines.length-1].to = index_end+2;
        $scope.payload.push({ "tickets": block.tickets });
      }
      if (!$scope.blockLines[$scope.blockLines.length-1].to)
        $scope.blockLines.pop();
      console.dir($scope.payload);
      console.dir($scope.blockLines);
      $scope.processIndex = 0;
      sendTicketsRequest();
    }

    function createUsers(csv, user_fields, columns, index_start, index_end) {
      $scope.payload.length = 0;
      $scope.blockLines.length = 0;
      let data, block = { "users": [] };
      $scope.blockLines = [{from:index_start + 2}];
      for (let i in csv) {
        if (Object.keys(csv[i]).length != $scope.CSVHeaders.length) continue;
        if ((i >= (index_start)) && (i <= (index_end))) {
          block.users.push(getUserBody(csv[i], columns, user_fields));
        }
        if (block.users.length == 100) {
          $scope.blockLines[$scope.blockLines.length-1].to = Number(i)+2;
          $scope.blockLines.push({from:Number(i)+3})
          $scope.payload.push({ "users": block.users });
          block = { "users": [] }
        }
      }
      if (block.users.length) {
        $scope.blockLines[$scope.blockLines.length-1].to = index_end+2;
        $scope.payload.push({ "users": block.users });
      }
      if (!$scope.blockLines[$scope.blockLines.length-1].to) {
        $scope.blockLines.pop();
      }
      console.dir($scope.payload);
      console.dir($scope.blockLines);
      $scope.processIndex = 0;
      $scope.endpoint = $scope.usersMethod == "updateOnly" ? '/updateusers' : '/createorupdateusers';
      sendUsersRequest();
    }

    function filterExistingUsers(csv) {
      return new Promise((resolve, reject) => {
        if ($scope.usersMethod == "createOrUpdate") {
          console.log("create user");
          return resolve(csv)
        }
        let search = '';
        for (let i in csv.users) {
          if (csv.users[i].email) {
            search += "email:" + csv.users[i].email + " ";
          } else if (csv.users[i].external_id) {
            search += "external_id:" + csv.users[i].external_id + " ";
          }
        }
        console.log(search);
        let data = {
          auth: $scope.zendesk,
          search: search
        }
        $http.post('/searchusers', data)
        .then(function(result) {
          console.log(result.data.users);
          let emails = result.data.users.map(function(item) {
            return item.email.toLowerCase();
          });

          let exIds = result.data.users.map(function(item) {
            return item.external_id;
          });

          csv.users = csv.users.filter(function(item) {
            if (item.external_id && ~exIds.indexOf(item.external_id)) {
              let index = exIds.indexOf(item.external_id);
              item.id = result.data.users[index].id;
              return true;
            } else if (item.email && ~emails.indexOf(item.email.toLowerCase())) {
              let index = emails.indexOf(item.email.toLowerCase());
              item.id = result.data.users[index].id;
              return true;
            }
            return  false;
          })
          resolve(csv);
        });
      });
    }

    function sendUsersRequest() {
      let foundUsers = 0;

      if (!$scope.payload[$scope.processIndex]) {
        setTimeout(function() {
          window.scrollTo(0, document.body.scrollHeight);
        }, 100);
        $scope.running = false;
        $scope.$apply();
        return $scope.cb();
      }

      setTimeout(function() {
        window.scrollTo(0, document.body.scrollHeight);
      }, 100);

      displayMessages("", "Executing step " + ($scope.processIndex + 1) + " of " + $scope.payload.length + "...");
      filterExistingUsers($scope.payload[$scope.processIndex])
      .then(function(result) {
        console.log(result);
        console.log($scope.blockLines[$scope.processIndex].from, 'Updating', result.users.length, 'users.');
        foundUsers = result.users.length || 0;
        let data = {
          auth: $scope.zendesk,
          users: result,
          log: "Creating users from row " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to
        };
        return result.users.length ? $http.post($scope.endpoint, data) : {data: 'nodata'};
      })
      .then(function(response) {
        console.log(response);
        if (response.data.error) {
          if (response.data.statusCode == 429) {
            displayMessages("red", "Too many jobs. Waiting 1 minute to retry...");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process && setTimeout(function(){sendUsersRequest()}, 60 * 1000);
          } else if ((response.data.error.code == "ECONNRESET") || (response.data.error.code == "ETIMEDOUT")) {
            displayMessages("red", "Connection error. Waiting 10 seconds to retry...");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process && setTimeout(function(){sendTicketsRequest()}, 10 * 1000);
          } else {
            displayMessages("red", "[Paused] Error while creating/updating users for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + "!");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process = false;
          }
        } else if (response.data == 'nodata') {
          displayMessages("gray", "No users found for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + "!");
        } else {
          displayMessages("", "<a href='" + response.data.job_status.url + "' target='_blank'>" + foundUsers + " users for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + " will be created/updated.</a>");
        }
        $scope.processIndex++;
        return $scope.update_process && setTimeout(function(){sendUsersRequest()}, $scope.delay * 1000);
      })
      .catch(function(err) {
        console.error(err);
        displayMessages("red", "[Paused] Error while creating/updating users for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + "!");
        setTimeout(function() {
          window.scrollTo(0, document.body.scrollHeight);
        }, 100);
        return $scope.update_process = false;
      });
    }

    function sendTicketsRequest() {
      if (!$scope.payload[$scope.processIndex]) {
        setTimeout(function() {
          window.scrollTo(0, document.body.scrollHeight);
        }, 100);
        $scope.running = false;
        $scope.$apply();
        return $scope.cb();
      }

      let data = {
        auth: $scope.zendesk,
        tickets: $scope.payload[$scope.processIndex],
        log: "Creating tickets for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to
      };

      setTimeout(function() {
        window.scrollTo(0, document.body.scrollHeight);
      }, 100);

      displayMessages("", "Executing step " + ($scope.processIndex + 1) + " of " + $scope.payload.length + "...");
      $http.post('/createtickets', data)
      .then(function(response) {
        console.log(response);
        if (response.data.error) {
          if (response.data.statusCode == 429) {
            displayMessages("red", "Too many jobs. Waiting 1 minute to retry...");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process && setTimeout(function(){sendTicketsRequest()}, 60 * 1000);
          } else if ((response.data.error.code == "ECONNRESET") || (response.data.error.code == "ETIMEDOUT")) {
            displayMessages("red", "Connection error. Waiting 10 seconds to retry...");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process && setTimeout(function(){sendTicketsRequest()}, 10 * 1000);
          } else {
            displayMessages("red", "[Paused] Error while creating tickets for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + "!");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process = false;
          }
        } else {
          displayMessages("", "<a href='" + response.data.job_status.url + "' target='_blank'>Tickets for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + " will be created.</a>");
        }
        $scope.processIndex++;
        return $scope.update_process && setTimeout(function(){sendTicketsRequest()}, $scope.delay * 1000);
      })
      .catch(function(err) {
        console.error(err);
        displayMessages("red", "[Paused] Error while creating tickets for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + "!");
        setTimeout(function() {
          window.scrollTo(0, document.body.scrollHeight);
        }, 100);
        return $scope.update_process = false;
      });
    }

    function sendOrgsRequest() {
      if (!$scope.payload[$scope.processIndex]) {
        setTimeout(function() {
          window.scrollTo(0, document.body.scrollHeight);
        }, 100);
        $scope.running = false;
        $scope.$apply();
        return $scope.cb();
      }

      let data = {
        auth: $scope.zendesk,
        tickets: $scope.payload[$scope.processIndex],
        log: "Creating organizations for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to
      };

      setTimeout(function() {
        window.scrollTo(0, document.body.scrollHeight);
      }, 100);

      displayMessages("", "Executing step " + ($scope.processIndex + 1) + " of " + $scope.payload.length + "...");
      $http.post('/createorganizations', data)
      .then(function(response) {
        console.log(response);
        if (response.data.error) {
          if (response.data.statusCode == 429) {
            displayMessages("red", "Too many jobs. Waiting 1 minute to retry...");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process && setTimeout(function(){sendOrgsRequest()}, 60 * 1000);
          } else if ((response.data.error.code == "ECONNRESET") || (response.data.error.code == "ETIMEDOUT")) {
            displayMessages("red", "Connection error. Waiting 10 seconds to retry...");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process && setTimeout(function(){sendOrgsRequest()}, 10 * 1000);
          } else {
            displayMessages("red", "[Paused] Error while creating organizations for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + "!");
            setTimeout(function() {
              window.scrollTo(0, document.body.scrollHeight);
            }, 100);
            return $scope.update_process = false;
          }
        } else {
          displayMessages("", "<a href='" + response.data.job_status.url + "' target='_blank'>Organizations for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + " will be created.</a>");
        }
        $scope.processIndex++;
        return $scope.update_process && setTimeout(function(){sendOrgsRequest()}, $scope.delay * 1000);
      })
      .catch(function(err) {
        console.error(err);
        displayMessages("red", "[Paused] Error while creating organizations for rows " + $scope.blockLines[$scope.processIndex].from + " to " + $scope.blockLines[$scope.processIndex].to + "!");
        setTimeout(function() {
          window.scrollTo(0, document.body.scrollHeight);
        }, 100);
        return $scope.update_process = false;
      });
    }

    function getTicketBody(ticket, columns, ticket_custom_fields) {
      let ticket_id, created_at, status, requester_name, requester_email,
      updated_at, ticket_subject, ticket_description, additional_tags, tags,
      tag_status, tag_id, ticketBody, custom_fields, group_id,
      createdAt, updatedAt, createdAtString, updatedAtString;

      columns = columns || {};
      try {
        group_id = columns.group;
        ticket_id = ticket[columns.id] || '';
        status_tag = ticket[columns.status_tag];
        status = columns.status.indexOf("fixed:") == 0 ? columns.status.replace("fixed:", "") : ticket[columns.status];
        requester_name = ticket[columns.requester_name] || '';
        requester_email = ticket[columns.requester_email] || '';
        created_at = ticket[columns.created_at];
        updated_at = ticket[columns.updated_at];
        ticket_subject = ticket[columns.subject] || 'Ticket migrado';
        additional_tags = [];

        createdAt = created_at ? moment(created_at, columns.date_format).toDate().toISOString().replace(/\.\d+/, '') : '';
        updatedAt = updated_at ? moment(updated_at, columns.date_format).toDate().toISOString().replace(/\.\d+/, '') : '';
        createdAtString = created_at ? moment(created_at, columns.date_format).format('DD/MM/YYYY HH:mm:ss') : '';
        updatedAtString = updated_at ? moment(updated_at, columns.date_format).format('DD/MM/YYYY HH:mm:ss') : '';

        tag_status = columns.status_tag ? "status_" + status_tag.replace(/\W/g, '_') : "";
        tag_id = columns.id ? "atendimento_" + columns.id.replace(/\W/g, "_") + "_" + ticket_id : "atendimento_sem_id";

        tags = columns.tags ? columns.tags.match(/\w+/g) : "";
        if (tags) {
          additional_tags = tags;
        }
        if (tag_status) {
          additional_tags.push(tag_status);
        }
        additional_tags.push(tag_id);

        ticket_description = "";
        for (let i in ticket) {
          if (createdAtString && i == columns.created_at) {
            ticket_description += i + ": " + createdAtString + "\n";
          } else if (updatedAtString && i == columns.updated_at) {
            ticket_description += i + ": " + updatedAtString + "\n";
          } else {
            ticket_description += i + ": " + ticket[i] + "\n";
          }
        }

        requester_email = requester_email.replace(/[^\w@.]/g, '');

        if (!requester_name.match(/\w+/))
          requester_name = "";
        if (!requester_email.match(/\w+@\w+.\w+/))
          requester_email = "";

        if (requester_name == "")
          requester_name = "semnome"
        if (requester_email == "")
          requester_email = requester_name.replace(/[^\w@.]/g, '') + "@sememail.com"

        custom_fields = [];
        for (let i in ticket_custom_fields) {
          custom_fields.push({ id: ticket_custom_fields[i].id, value: ticket[ticket_custom_fields[i].value] })
        }

        ticketBody = {
          "tags": additional_tags,
          "status": status,
          "requester": { "name": requester_name, "email": requester_email },
          "subject": ticket_subject,
          "comment": { "body": ticket_description, "public": true },
          "description": ticket_description,
          "custom_fields": custom_fields
        };

        if (created_at)
          ticketBody.created_at = createdAt;
        if (updated_at)
          ticketBody.updated_at = updatedAt;

        if (group_id) {
          ticketBody.group_id = group_id;
        }
      } catch (err) {
        console.error("Error generating Ticket Body.", err)
        ticketBody = {}
      }

      return ticketBody;
    };

    function getUserBody(user, columns, user_custom_fields) {
      let userBody = {},
      user_fields = {};

      columns = columns || {};
      try {
        for (let property in columns) {
          if (user[columns[property]]) {
            if ((property == "organization_id") && (isNaN(user[columns[property]]))) {
              userBody["organization"] = {name: user[columns[property]]};
            } else {
              userBody[property] = user[columns[property]];
            }
          } else if (columns[property].indexOf("fixed:") == 0) {
            userBody[property] = columns[property].replace("fixed:", "");
          }
        }

        if (columns.default_group_id) {
          userBodydefault_group_id = columns.default_group_id;
        }

        for (let i in user_custom_fields) {
          let value = user[user_custom_fields[i].value];
          if (user_custom_fields[i].dataType == 'Date_br') {
            value = moment(user[user_custom_fields[i].value], 'DD/MM/YYYY').format();
          }
          user_fields[user_custom_fields[i].key] = value;
        }

        userBody.user_fields = user_fields;
      } catch (err) {
        console.error("Error generating User Body.", err)
        userBody = {}
      }

      return userBody;
    };

    function getOrgBody(org, columns, org_custom_fields) {
      let orgBody = {}, org_fields = {};

      columns = columns || {};
      try {
        for (let property in columns) {
          if (org[columns[property]])
            orgBody[property] = org[columns[property]];
        }

        if (columns.domain_names) {
          orgBody.domain_names = columns.domain_names.match(/[\w\.]+/g);
        }
        if (columns.tags) {
          orgBody.tags = columns.tags.match(/\w+/g);
        }
        if (columns.group) {
          orgBody.group_id = columns.group;
        }

        for (let i in org_custom_fields) {
          let value = org[org_custom_fields[i].value];
          if (org_custom_fields[i].dataType == 'Date_br') {
            value = moment(org[org_custom_fields[i].value], 'DD/MM/YYYY').format();
          }
          org_fields[org_custom_fields[i].key] = value;
        }

        orgBody.organization_fields = org_fields;
      } catch (err) {
        console.error("Error generating Org Body.", err)
        orgBody = {}
      }

      return orgBody;
    };

    function displayMessages(style, message) {
      $("body").append("<b><p style='color:" + style + "';>" + message + "</p></b>");
  //$("html, body").animate({ scrollTop: $(document).height() }, 100);
}

function displayExtraMessages(style, message) {
  $(".btns-control").append("<b><p style='color:" + style + "';>" + message + "</p></b>");
  //$("html, body").animate({ scrollTop: $(document).height() }, 100);
}

function dateParserBR(date) {
  let pos = date.indexOf('/');
  let day = date.substring(0, date.indexOf('/'));
  let month = date.substring(pos + 1, date.indexOf('/', pos + 1));
  let year = date.substring(date.length - 4, date.length);
  return (day + month + year);
}

});
