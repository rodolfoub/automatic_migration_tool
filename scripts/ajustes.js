 const config = require('./config.json');
const rp = require('request-promise');

let getUrl = '/users/search?query=Juliano Mossi';
let actUrl = '/users/update_many.json';
let actType = 'PUT';
let contentArray = 'users';
let bodyKey = 'user';
let bodyContent = {"organization": {"name": "Aktie Now"}};

console.log('Executando script em: ' + config.uri);

function zendeskAPI(method, endpoint, data) {
  let request = {
    method: method,
    uri: config.uri + '/api/v2' + endpoint,
    headers: {
      'Authorization': 'Basic ' + new Buffer(config.user + '/token:' + config.APIkey).toString('base64'),
      'Accept': 'application/json'
    },
    body: data,
    json: true
  }
  return rp(request);
}

function processTickets(url) {
  return new Promise((resolve, reject) => {
    let next_page = '', count = 0;
    zendeskAPI('GET', url)
      .then(function(response) {
        count = response.count !== null ? response.count : 0;
        next_page = response.next_page && response.next_page !== null ? response.next_page.replace(/https.*\/v2/, '') : '';
        let ids = response[contentArray].map(function(item) {
          return item.id;
        });

        console.log(ids.toString());
        console.log('Restantes: ' + count);

        let body = {
          ids: ids.toString()
        };

        if (bodyContent) {
          body[bodyKey] = bodyContent;
        }

        console.log(actType, actUrl, body);

        return ids.length ? zendeskAPI(actType, actUrl, body) : [];
      })
      .then(function(response) {
        console.log(response);
        resolve(next_page);
      })
      .catch(function(err) {
        reject(err);
      });
  });
}

function init(page, count) {
  console.log('Processando página:' + page);
  processTickets(page)
  .then(function(next_page) {
    if (next_page !== '') {
      init(next_page)
    } else {
      console.log('Finalizado!');
    }
  })
  .catch(function(err) {
    console.error(err);
    setTimeout(function() {
      init(page, count);
    }, 60 * 1000);
  });
}

init(getUrl);
