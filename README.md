# Endereço

* http://ec2-18-231-72-25.sa-east-1.compute.amazonaws.com:4000/

# Utilização

* Acesse o endereço através de um navegador.
* Preencha os campos correspondentes ao ambiente Zendesk.
* Selecione uma das opções de execução.
* Na tela seguinte, selecione o arquivo CSV que contém os dados.
* Preencha os campos conforme desejado, indicando as colunas correspondentes quando necessário.
* É possível incluir campos customizados.

* Clique em OK para iniciar a execução.
* É possível pausar a execução.
* A execução é pausada automaticamente caso ocorra algum erro inesperado.

# Notas

* Em Tickets, ao selecionar uma coluna para _Created at_ ou _Updated at_ a aplicação irá converter
o conteúdo da coluna para o formato 01/01/1999 00:00:00 no corpo do ticket. Para tanto, é necessário
informar o formato em que a coluna está codificada, utilizando o formato da biblioteca _moment_, respeitando
maiúsculas e minúsculas: _YYYY_ = Ano; _MM_ = Mês; _DD_ = Dia; _HH_ = Horas; _mm_ = Minutos; _ss_ = Segundos.
