var express = require('express');
var router = express.Router();
var path = require('path');
var zendesk = require('../modules/zendesk.js')

//works only with there's no public/index.html
router.get('/', function(req, res, next) {
  res.send("CSV Automation")
});

router.post('/auth', function(req, res, next) {
  zendesk.auth(req.body)
    .then(function(response) {
      res.send(response);
    })
    .catch(function(error) {
      res.send(error);
    });
});

router.post('/searchusers', function(req, res, next) {
  zendesk.search_users(req.body)
    .then(function(response) {
      res.send(response);
    })
    .catch(function(error) {
      res.send(error);
    });
});

router.post('/updateuser', function(req, res, next) {
  zendesk.update_user(req.body)
    .then(function(response) {
      res.send(response);
    })
    .catch(function(error) {
      res.send(error);
    });
});

router.post('/updateusers', function(req, res, next) {
  console.log(req.body.log);
  zendesk.update_many_users(req.body)
    .then(function(response) {
      console.log(response.job_status.url);
      res.send(response);
    })
    .catch(function(error) {
      console.error(error.error.description);
      res.send(error);
    });
});

router.post('/createorupdateusers', function(req, res, next) {
  console.log(req.body.log);
  zendesk.create_or_update_many_users(req.body)
    .then(function(response) {
      console.log(response.job_status.url);
      res.send(response);
    })
    .catch(function(error) {
      console.error(error.error.description);
      res.send(error);
    });
});

router.post('/createtickets', function(req, res, next) {
  console.log(req.body.log);
  zendesk.create_many_tickets(req.body)
    .then(function(response) {
      console.log(response.job_status.url);
      res.send(response);
    })
    .catch(function(error) {
      console.error(error.error.description);
      res.send(error);
    });
});

router.post('/createorganizations', function(req, res, next) {
  console.log(req.body.log);
  zendesk.create_many_organizations(req.body)
    .then(function(response) {
      console.log(response.job_status.url);
      res.send(response);
    })
    .catch(function(error) {
      console.error(error.error.description);
      res.send(error);
    });
});


module.exports = router;
