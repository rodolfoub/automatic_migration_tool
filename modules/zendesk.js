const rp = require('request-promise'),
  fs = require('fs');

function zendeskAPI(method, endpoint, auth, data) {
  let request = {
    method: method,
    uri: auth.uri + '/api/v2' + endpoint,
    headers: {
      'Authorization': 'Basic ' + new Buffer(auth.user + '/token:' + auth.APIkey).toString('base64'),
      'Accept': 'application/json'
    },
    body: data,
    json: true
  }
  return rp(request);

}

let zendesk = {
  auth: function(auth) {
    return zendeskAPI('GET', '/users/me.json', auth);
  },

  create_many_tickets: function(data) {
    return zendeskAPI('POST', '/imports/tickets/create_many.json', data.auth, data.tickets);
  },

  create_many_organizations: function(data) {
    return zendeskAPI('POST', '/organizations/create_many.json', data.auth, data.tickets);
  },

  search_users: function(data) {
    return zendeskAPI('GET', '/users/search.json?query=' + data.search, data.auth);
  },

  update_many_users: function(data) {
    return zendeskAPI('PUT', '/users/update_many.json', data.auth, data.users);
  },

  create_or_update_many_users: function(data) {
    return zendeskAPI('POST', '/users/create_or_update_many.json', data.auth, data.users);
  },

  new_client: function() {
    clientZendesk = require('node-zendesk').createClient({
      username: config.zendesk.username,
      token: APIkeys.zendesk,
      remoteUri: config.zendesk.remoteUri
    });
    return clientZendesk;
  },


  get_user: function(id, cb) {
    return zendeskAPI("GET", "/users/" + id, '');
  },


  get_tickets: function(cb, url) {
    let endpoint = (url === undefined) ? "/search.json?query=type:ticket tags:tipo_de_solicitacao_trabalho_remoto status<pending" : url;
    return zendeskAPI("GET", endpoint, '');
  }
}

module.exports = zendesk;
